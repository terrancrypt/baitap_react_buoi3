import React, { Component } from "react";
import Item_Shoe from "./Item_Shoe";

export default class List_Shoe extends Component {
  render() {
    return (
      <div className="row">
        {this.props.listShoe.map((shoe) => {
          return <Item_Shoe item={shoe} handleOnClick={this.props.handleAddToCart}/>;
        })}
      </div>
    );
  }
}
