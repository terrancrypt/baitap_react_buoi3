import React, { Component } from "react";

export default class Cart extends Component {
  renderCart() {
    return this.props.data.map((item, index) => {
      return (
        <tr>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => this.props.handleChangeQuantity(index, "giam")}
              className="btn btn-danger"
            >
              -
            </button>
            <strong className="m-3"> {item.soLuong}</strong>
            <button
              onClick={() => this.props.handleChangeQuantity(index, "tang")}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => this.props.handleRemoveItem(index)}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="p-4 border">
        <table className="table text-center">
          <thead>
            <th>Image</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Function</th>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}
