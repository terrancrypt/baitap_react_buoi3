import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./Data_shoe";
import List_Shoe from "./List_Shoe";

export default class Shoe_Shop extends Component {
  state = {
    listShoe: dataShoe,
    cartData: [],
  };

  handleAddToCart = (shoe) => {
    // Tạo một biến cloneCart kế thừa từ biến cartData để lấy dữ liệu
    let cloneCart = [...this.state.cartData];

    // Tìm ra vị trí của sản phẩm trong cloneCart, nếu chưa có thì push sản phẩm vô với số lượng = 1, nếu có rồi thì push số lượng + 1
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });

    // Nếu index = -1 thì chưa có, còn nếu bằng 0 thì có rồi
    if (index == -1) {
      // Tạo ra một đối tượng mới có số lượng để push vào mảng
      let shoeInCart = {
        ...shoe,
        soLuong: 1,
      };
      cloneCart.push(shoeInCart);
    } else {
      // Ngược lại nếu sẩn phẩm đã có trong cart rồi thì không cần phải push nữa mà chỉ up số lượng lên thôi
      cloneCart[index].soLuong++;
    }

    // cartData là thứ sẽ thay đổi nên setState cho nó
    this.setState({
      cartData: cloneCart,
    });
  };

  handleChangeQuantity = (index, hanhDong) => {
    let cloneCartData = [...this.state.cartData];

    if (hanhDong === "tang") {
      cloneCartData[index].soLuong += 1;
    } else if (hanhDong === "giam") {
      cloneCartData[index].soLuong -= 1;

      // Nếu số lượng = 0 thì xóa sản phẩm khỏi giỏ hàng
      if (cloneCartData[index].soLuong === 0) {
        cloneCartData.splice(index, 1);
      }
    }

    this.setState({
      cartData: cloneCartData,
    });
  };

  handleRemoveItem = (id) => {
    let cloneCartData = [...this.state.cartData];

    cloneCartData.splice(id, 1);
    
    this.setState({
        cartData: cloneCartData,
      });
  };

  render() {
    return (
      <div className="container py-5">
        <h1>SHOE SHOP</h1>
        <div>
          <Cart
            data={this.state.cartData}
            handleChangeQuantity={this.handleChangeQuantity}
            handleRemoveItem={this.handleRemoveItem}
          />
        </div>
        <div>
          <List_Shoe
            listShoe={this.state.listShoe}
            handleAddToCart={this.handleAddToCart}
          />
        </div>
      </div>
    );
  }
}
