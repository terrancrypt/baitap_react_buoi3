import React, { Component } from "react";

export default class Item_Shoe extends Component {
  render() {
    let { name, price, image } = this.props.item;
    return (
      <div className="col-3 p-2">
        <div className="card">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">$ {price}</p>
            <a
              href="#"
              className="btn btn-primary"
              onClick={() => this.props.handleOnClick(this.props.item)}
            >
              Add to cart
            </a>
          </div>
        </div>
      </div>
    );
  }
}
